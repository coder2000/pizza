package ca.dieterlunn.android.pizza.callbacks;

/**
 * Created by Dieter on 12/22/2014.
 */
public interface NavigationDrawerCallbacks {
    void onNavigationDrawerItemSelected(int position);
}
